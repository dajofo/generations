import { calcWeightedRandom } from 'utils/calculations';

export default class Person {
  constructor(id = 0, parentID = -1, birthYear = 0) {
  	this.id = id;
  	this.parentID = parentID;
  	this.birthYear = birthYear;
  	this.deathYear = this.birthYear + calcWeightedRandom(110, 2.5); // (max age, weight)
    this.childrenBirthYears = this.getChildrenBirthYears();
  }

  getChildrenBirthYears() {
  	let possibleChildren = calcWeightedRandom(6, 2); // (max children, weight)
  	if (this.parentID == -1 && possibleChildren == 0) {
  		possibleChildren = 2; // can't let the family tree be empty!
  	}
  	const youngestFertileAge = 18;
  	const oldestFertileAge = 40;

  	let birthYears = [];
  	for (let i = youngestFertileAge; i < oldestFertileAge; i++) {
  		if (possibleChildren > 0) {
  			if (Math.random() < 0.25) {
  				birthYears.push(this.birthYear + i);
  				possibleChildren--;
  			}
  		} else {
  			break;
  		}
  	}
  	return birthYears;
  }

  getAge() {
  	return this.deathYear - this.birthYear;
  }
}