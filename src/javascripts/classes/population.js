import Person from './person';

export default class Population {
	constructor(yearsToTravel = 100, startYear = 0) {
		this.startYear = startYear;
		this.endYear = yearsToTravel + startYear;
		this.currentYear = startYear;
		this.currentPersonID = 0;
		this.population = this.generatePopulation();
		delete this.currentPersonID;
		delete this.currentYear;
	}

	generatePopulation() {
		let population = {
			isFertile: [ new Person(0, -1, this.startYear) ],
			notFertile: []
		};

		// Loop through years.
		for (this.currentYear; this.currentYear < this.endYear; this.currentYear++) {
			// Sort the isFertile list by first year in everyone's childrenBirthYears list
			population.isFertile.sort( (a,b) => a.childrenBirthYears[0] - b.childrenBirthYears[0] );
			// Loop through fertile people
			for (let i = 0; i < population.isFertile.length; i++) {
				let person = population.isFertile[i];
				// If due to give birth this year...
				if (person.childrenBirthYears[0] == this.currentYear) {
					this.currentPersonID++;
					let child = new Person(this.currentPersonID, person.id, this.currentYear);
					if (child.childrenBirthYears.length > 0) {
						population.isFertile.push(child);
					} else {
						population.notFertile.push(child);
					}
					person.childrenBirthYears.shift(); // child created, so remove their birth year from parent

					// No more births for you?
					if (person.childrenBirthYears.length == 0) {
						population.notFertile.push(person); // add to notFertile list
						population.isFertile.splice(i, 1); // remove from isFertile list
					}
				}
			}
		}
		const everyone = population.isFertile.concat(population.notFertile);
		everyone.sort((a,b) => a.birthYear - b.birthYear);
		return everyone;
	}
}