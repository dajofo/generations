function calcHypotenuse(a, b) {
  return(Math.sqrt( calcSquare(a) + calcSquare(b) ));
}

function calcSquare(a) {
	return a * a;
}

// Returns random boolean
function coinFlip() {
	return Math.floor(Math.random() * 2) == 0;
}

// returns random int weighted toward half the max.
// increated weight makes it more likely to be close to half of max
function calcWeightedRandom(max, weight) {
    let num = 0;
    for (let i = 0; i < weight; i++) {
        num += Math.random() * (max / weight);
    }    
    return parseInt(num);
}

function calcRandomDegree(decimalPlaces = 0) {
	return (Math.random() * 360).toFixed(decimalPlaces);
}

export {calcHypotenuse, calcSquare, coinFlip, calcWeightedRandom, calcRandomDegree}