import 'modules';
import Population from './classes/population';
import { calcRandomDegree } from 'utils/calculations';

let startTime = new Date();
// --------------------------------------

const config = {
	animationDuration: 15000, // milliseconds
	years: 400, // beware over 500
	startYear: 0
};
config.yearDurationRatio = (config.years / config.animationDuration).toFixed(4);


let familyTree = new Population(config.years, config.startYear);
let people = familyTree.population;
console.log('******** ' + people.length + ' people ********');

const elContainer = document.getElementById('points-container');

for (let person of people) {
	let elPerson = makePersonEl( people.shift() );
	elContainer.appendChild(elPerson);
}

function makePersonEl(person) {
	// Point Container
	const width = 'width: ' + Math.floor((person.birthYear / config.years) * 50) + 'vmin;';
	const transform = 'transform: rotate(' + calcRandomDegree(2) + 'deg);';
	
	let elMarkerContainer = document.createElement('div');
	elMarkerContainer.className = 'point-container';
	elMarkerContainer.setAttribute('style', width + transform);

	// Point
	const cssAnimationDelay = 'animation-delay: ' + Math.floor((person.birthYear / config.years) * config.animationDuration) + 'ms;';
	const cssAnimationDuration = 'animation-duration: ' + Math.floor(person.getAge() / config.yearDurationRatio) + 'ms;';
	
	let elMarker = document.createElement('symbol');
	elMarker.className = 'point';
	elMarker.setAttribute('style', cssAnimationDelay + cssAnimationDuration);

	elMarkerContainer.appendChild(elMarker);
	return elMarkerContainer;
}


// --------------------------------------
let duration = new Date() - startTime;
console.log(config);
console.log('runtime: ' + duration + 'ms');


